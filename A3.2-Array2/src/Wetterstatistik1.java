import java.lang.reflect.Array;
import java.util.Arrays;

public class Wetterstatistik1 {

	public static void main(String[] args) {

		double[] temperatur = { 4.0, 3.2, 2.5, 1.9, 1.6, 1.1, 0.3, 0.1, 1.0, 4.9, 8.8, 13.5, 14.1, 15.3, 16.3, 17.1,
				17.6, 18.2, 17.2, 15.8, 13.5, 13.2, 13.3, 12.0 };

		System.out.printf("Der ø Wert liegt bei: %11.1f\n", berechneMittelwert(temperatur));
		System.out.printf("Der kleinste Wert liegt bei: %4.1f\n", berechneMinimum(temperatur));
		System.out.printf("Der höchste Wert liegt bei: %5.1f", berechneMaximum(temperatur));

	}

	public static float berechneMittelwert(double[] temperatur) {

		float ausgabe = 0;

		for (int i = 0; i < temperatur.length; i++) {

			ausgabe = (float) (ausgabe + temperatur[i]);

		}

		return (int) (ausgabe / temperatur.length);

	}

	public static double berechneMaximum(double[] temperatur) {

		Arrays.sort(temperatur);

		double ausgabe = temperatur[temperatur.length - 1];

		return ausgabe;
	}

	public static double berechneMinimum(double[] temperatur) {

		Arrays.sort(temperatur);

		double ausgabe = temperatur[0];

		return ausgabe;
	}

}
