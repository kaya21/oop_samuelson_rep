import java.util.Arrays;
import java.util.Scanner;

public class Wetterstatistik2 {

	static Scanner myScanner = new Scanner(System.in);

	public static void main(String[] args) {

		double[] temperatur = { 4.0, 3.2, 2.5, 1.9, 1.6, 1.1, 0.3, 0.1, 1.0, 4.9, 8.8, 13.5, 14.1, 15.3, 16.3, 17.1,
				17.6, 18.2, 17.2, 15.8, 13.5, 13.2, 13.3, 12.0 };

		menueAusgabe(berechneMittelwert(temperatur), berechneMinimum(temperatur), berechneMaximum(temperatur));

	}

	public static double berechneMittelwert(double[] temperatur) {

		float ausgabe = 0;

		for (int i = 0; i < temperatur.length; i++) {

			ausgabe = (float) (ausgabe + temperatur[i]);

		}

		return (int) (ausgabe / temperatur.length);

	}

	public static double berechneMaximum(double[] temperatur) {

		Arrays.sort(temperatur);

		double ausgabe = temperatur[temperatur.length - 1];

		return ausgabe;
	}

	public static double berechneMinimum(double[] temperatur) {

		Arrays.sort(temperatur);

		double ausgabe = temperatur[0];

		return ausgabe;
	}

	public static void menueAusgabe(double dwert, double min, double max) {

		System.out.println("+-------------------------+");
		System.out.println("|      (M)ittelwert       |");
		System.out.println("|       M(i)nimum         |");
		System.out.println("|       Ma(x)imum         |");
		System.out.println("+-------------------------+");

		System.out.println("|       Be(e)nden         |");
		System.out.println("+-------------------------+");

		System.out.println("Treffen Sie eine Auswahl:");

		char eingabe = myScanner.next().charAt(0);

		switch (eingabe) {

		case 'm':

			System.out.printf("       Mittelwert: %5.1f    ", dwert);
			break;

		case 'i':

			System.out.printf("       Minimum: %5.1f    ", min);
			break;

		case 'x':

			System.out.printf("       Maximum: %5.1f    ", max);
			break;

		case 'e':
			System.out.printf("       Vorgang beendet    ");
			break;

		}

	}

}
