import java.util.Scanner;

public class Verschluesselung1 {

	static int offset = 10;

	static Scanner myScanner = new Scanner(System.in);
	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		char[] meinArray = encrypt(einlesenPasswort("Bitte geben Sie ein Passwort ein:"), offset);

		decrypt(meinArray, offset);

	}

	public static char[] einlesenPasswort(String ausgabe) {

		System.out.println(ausgabe);

		String passwort = scanner.nextLine();

		char[] passArr = passwort.toCharArray();

		return passArr;

	}

	public static char[] encrypt(char[] eingabe, int offset) {

		char[] cryptArr = new char[eingabe.length];

		System.out.println("Verschlüsseltes Passwort:");

		for (int i = 0; i < eingabe.length; i++) {

			int verschiebung = (eingabe[i] + offset);

			cryptArr[i] = (char) (verschiebung);

			System.out.print(cryptArr[i]);
		}

		return cryptArr;

	}

	public static char[] decrypt(char[] eingabe, int offset) {

		char[] decryptArr = new char[eingabe.length];

		System.out.println("\n\nEntsschlüsselt:");

		for (int i = 0; i < eingabe.length; i++) {
			decryptArr[i] = (char) (eingabe[i] - offset);

			System.out.print(decryptArr[i]);

		}

		return decryptArr;

	}

}
