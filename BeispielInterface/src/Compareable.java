
public interface Compareable<T> { // T ist Platzhalter für DatenTyp
	
	
	/*
	 * liefert >0, wenn größer
	 * liefert 0, wenn gleich
	 * liefert <0, wenn kleiner
	 * 
	 */

	int compareTo(T o);	// T oben gewählter Datentyp, o steht für Objekt
}
