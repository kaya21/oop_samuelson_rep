
public class Kreis extends GeometrischeObjekte implements Form{
	
	private double radius;
	
	
	

	public Kreis(String farbe, int xpos, int ypos, double radius) { //Konstruktor
		super(farbe, xpos, ypos);
		this.radius = radius;
	}
	
	@Override
	public void zeichne() {
		
		System.out.println("Kreis zeichenen");
	}
	
	
	@Override
	public String toString() {
		return super.toString() + ", " +radius;
	}
	
	@Override
	public double berechneFlaeche() {
		return Form.PI*radius*radius;
	}
	
	@Override
	public double berechneUmfang() {
		return 2*Form.PI*radius;
	}

}
