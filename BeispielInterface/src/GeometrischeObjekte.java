
public abstract class GeometrischeObjekte {// Abstrakte Klasse, Objekte können nicht erstellt werden
	protected String farbe;
	protected int xpos;
	protected int ypos;
	
	
	
	public GeometrischeObjekte(String farbe, int xpos, int ypos) {		//Konstruktor
		super();
		this.farbe = farbe;
		this.xpos = xpos;
		this.ypos = ypos;
	}
	
	public abstract void zeichne();			//Abstrakte Methode
	
	public void move(int x, int y) {
		xpos += x;
		ypos += y;
		
		
	}
	
	@Override
	public String toString() {
		return this.farbe + ", (" +xpos +"," +ypos+ ")";
	}
	
	
	

}
