public class Person implements Comparable<Person>{
	
	private int personalnummer;
	private String name;
	

	public Person(String name, int personalnummer) {
		setName(name);
		setPersonalnummer(personalnummer);
	}
	
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getPersonalnummer() {
		return personalnummer;
	}
	public void setPersonalnummer(int personalnummer) {
			this.personalnummer = personalnummer;
	}
	
	public String toString() {   //  [ Name: Max , Personalnummer: 123456 ]
		return "[ Name: " + this.name + " , Alter: " + this.personalnummer + " ]";
	}
	
	@Override
	public int compareTo(Person p) {
		if(this.personalnummer < p.getPersonalnummer())
			return -1;
		if(this.personalnummer > p.getPersonalnummer())
			return 1;
		
		return 0;
		
		
	}
}
