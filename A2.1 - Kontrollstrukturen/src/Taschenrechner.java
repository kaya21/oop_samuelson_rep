
import java.util.Scanner;

public class Taschenrechner {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.printf("Bitte geben Sie die erste Zahl ein:\n");

		float zahl1 = myScanner.nextFloat();

		System.out.printf("Bitte geben Sie die zweite Zahl ein:\n");

		float zahl2 = myScanner.nextFloat();

		System.out.printf("Welche Rechenoperation wollen Sie durchführen\nWählen Sie zwischen:   + ,   -   ,   /\n");

		// char zeichen = myScanner.next().charAt(0);
		String zeichen = myScanner.next();

		if (zeichen.equals("+")) { // Strings werden mit equals verglichen

			System.out.printf("\n Ergebniss: %.2f\n", +(zahl1 + zahl2));
		}

		else if (zeichen.equals("-")) {

			System.out.printf("\n Ergebniss: %.2f\n", +(zahl1 - zahl2));
		}
		else if (zeichen.equals("/")) {

			System.out.printf("\n Ergebniss: %.2f\n", +(zahl1 / zahl2));
		}

		else {

			System.out.printf("Falsche Eingabe");

		}
	}

}