
public class Mittelwert {

	static double x;
	static double y;
	static double m;

	public static void main(String[] args) {

		// (E) "Eingabe"
		eingabenFestlegen();

		m = eingabenVerarbeiten(x, y);

		ausgabe(x, y, m);
	}

	public static double eingabenVerarbeiten(double zahl1, double zahl2) {

		return (zahl1 + zahl2) / 2.0;

	}

	public static void eingabenFestlegen() {

		x = 6.0;
		y = 4.0;

	}

	public static void ausgabe(double zahl1, double zahl2, double ergebnis) {

		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl1, zahl2, ergebnis);

	}

}
