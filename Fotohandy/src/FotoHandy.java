
public class FotoHandy extends Handy {
	private int pixel;
	private String firma;
	public static String klassenName = "FotoHandy";

	public FotoHandy(String firma1, String typ, int pixel, String firma2) {
		super(firma1, typ);
		this.pixel = pixel;
		this.firma = firma2;
	}

	public int getPixel() {
		return pixel;
	}

	public void setPixel(int pixel) {
		this.pixel = pixel;
	}

	public String getFirma() {
		return firma;
	}

public void setFirma(String firma) {
this.firma = firma;
}

public String toString() {
return "FotoHandy [firma(super)=" + super.getFirma() + ", typ=" + getTyp() +
", pixel=" + pixel + ", firma=" + firma + "]";

	}
}