
public class Handy {
	private String firma;
	private String typ;
	public static String klassenName = "Handy";

	public Handy(String firma, String typ) {
		this.firma = firma;
		this.typ = typ;
	}

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String toString() {
		return "Handy [firma=" + firma + ", typ=" + typ + "]";
	}
}
