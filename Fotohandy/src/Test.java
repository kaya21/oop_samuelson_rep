
public class Test {

	public static void main(String[] args) {
		Handy h = new Handy("Nokia1", "C62");
		System.out.println("h: " + h);
		FotoHandy f = new FotoHandy("Nokia1", "TS8", 5000000, "Nokia2");
		System.out.println("f: " + f);
		System.out.println("f.super: " + ((Handy) f).toString());
		System.out.println("f.klassenName: " + f.klassenName);
		System.out.println("f.klassenName: " + ((Handy) f).klassenName);
	}

}
/*
 * h: Firma: Nokia Typ: C62
 * f: Fotohandy...
 * f.super: 
 * 
 * 
 */
