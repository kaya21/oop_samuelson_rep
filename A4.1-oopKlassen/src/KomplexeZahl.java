
public class KomplexeZahl {

	private double re; // Atribute
	private double im; // Atribute

	public KomplexeZahl() {

		setRe(3);
		setIm(2);

	}

	public KomplexeZahl(double re, double im) {

		setRe(re);
		setIm(im);

	}

	public double getRe() {
		return re;
	}

	public void setRe(double re) {
		this.re = re;
	}

	public double getIm() {
		return im;
	}

	public void setIm(double im) {
		this.im = im;
	}
	@Override
	public String toString() {

		if (this.im >= 0) {
			return this.re + " +" + this.im + "j";
		} else
			return this.re + " " + this.im + "j";

	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj instanceof KomplexeZahl) {
			
			KomplexeZahl dtemp = (KomplexeZahl) obj;
			return this.re == dtemp.getRe() && this.im == dtemp.getIm();
		}
		return false;
			
		
	}
	//Objekt Methode
	public static KomplexeZahl multi(KomplexeZahl k1, KomplexeZahl k2) {
		
		double ergRe = k1.getRe()*k2.getRe() - k1.getIm()*k1.getIm();
		
		double ergIm = k1.getRe()*k2.getIm() + k1.getIm()*k2.getRe();
		
		return new KomplexeZahl(ergIm, ergRe);
	}
	
	//
	
	
	
	

}
