
public class Adresse {

	private String str;
	private String hausnummer;
	private String plz;
	private String ort;

	public Adresse(String str, String hausnummer, String plz, String ort) {

		this.str = str;
		this.hausnummer = hausnummer;
		this.plz = plz;
		this.ort = ort;

	}

	// [Straße: Steinstr. , Hausnummer

	@Override
	public String toString() {
		return "[Straße: " + this.str + ", Hausnummer: " + this.hausnummer + ", PLZ: " + this.plz + ", Ort: " + this.ort
				+ "]";

	}

	public String getStr() {

		return str;
	}

	public String getHNr() {

		return hausnummer;
	}
	
	public String getPLZ() {

		return plz;
	}
	
	public String getOrt() {

		return ort;
	}

}
