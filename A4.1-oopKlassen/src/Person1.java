
public class Person1 {

	private String name;
	private Datum geburtsdatum;
	
	private int personalnr;
	public static int anzahl = 0;

	public Person1(String name, Datum d1) {

		setPersonalnr(1000+anzahl);
		setName(name);
		this.geburtsdatum = d1;
		anzahl++;

	}

	@Override
	public String toString() {

		return "\n Nr.:" +personalnr+ "      Vorname: " + name + "      Geburtsdatum: "
				+ geburtsdatum.toString() + "\n";

	}
	
	
	

	public Datum getGeburtsdatum() {
		return geburtsdatum;
	}

	public void setGeburtsdatum(Datum geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}

	public int getPersonalnr() {
		return personalnr;
	}

	public void setPersonalnr(int nextNumber) {
		this.personalnr = nextNumber;
	}

	public int getAnzahl() {
		return anzahl;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
