
public class DatumTest {

	public static void main(String[] args) {

		Datum d1 = new Datum(02, 02, 1970); // zuweisung d2 des Konstruktors mit Atribute

		Datum d2 = new Datum(); // zuweisung d2 des Konstruktors ohne Atribute

		System.out.printf("%d.%d.%d\n", d1.getTag(), d1.getMonat(), d1.getJahr());

		System.out.println(d1.equals(d2)); // Vergleicht Atribute der Konstruktoren und gibt true or false aus

		System.out.println(d2.getJahr());
		System.out.println(d1);

		
		System.out.println(Datum.berechneQuartal(d1));
		System.out.println(Datum.berechneQuartal2(d1));
	}

}
