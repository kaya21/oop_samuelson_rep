
public class StaticDatum {
	
	private int tag;		// Atribute 
	private int monat;		// Atribute 
	private int jahr;		// Atribute 
	
	public StaticDatum() {

		setTag(1);
		setMonat(10);
		setJahr(1970);
	}
	
	/*set- und get Methoden*/

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) {
		this.monat = monat;
	}

	public int getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) {
		this.jahr = jahr;
	}
	
	public String toString() {
		return this.tag + "." + this.monat + "." + this.jahr;
	}
	
	
	

	
	
	
	

}
