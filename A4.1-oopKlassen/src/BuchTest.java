
public class BuchTest {

	public static void main(String[] args) {
		
		
		Buch b1 = new Buch("Einführung in OOP");		
		System.out.println(b1);
		
		Buch b2 = new Buch("Einführung in DB");
		System.out.println(b2);
		
		Buch b3 = new Buch("Einführung in PHP");
		System.out.println(b3);
		
		System.out.println(Buch.nextNumber);
		System.out.println(b3.getNextNumber());
		System.out.println(b3.toString());
	}

}
