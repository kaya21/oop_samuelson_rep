
public class Datum {

	private int tag; // Atribute
	private int monat; // Atribute
	private int jahr; // Atribute

	/* Konstruktor ohne Parameter */

	public Datum(int tag, int monat, int jahr) {

//		this.tag = tag;
//		this.monat = monat;
//		this.jahr = jahr;

		setTag(tag);
		setMonat(monat);
		setJahr(jahr);

	}
	/* Konstruktor mit festen Parameter */

	public Datum() {

		setTag(1);
		setMonat(1);
		setJahr(1970);
	}
	
	
	

	/* set- und get Methoden */

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) {
		this.monat = monat;
	}

	public int getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) {
		this.jahr = jahr;
	}

	/* to String Methode */

	public String toString() {
		return this.tag + "." + this.monat + "." + this.jahr;
	}

	/* equals Methode */

	@Override
	public boolean equals(Object obj) {

		if (obj instanceof Datum) {

			Datum dTemp = (Datum) obj;
			return this.tag == dTemp.getTag() && this.monat == dTemp.getMonat() && this.jahr == dTemp.getJahr();

		}
		return false;

	}

	public static int berechneQuartal2(Datum d) {

		

		if (d.getMonat() <= 3) {
			return 1;
		} else if (d.getMonat() <= 6 && d.getMonat() > 3) {
			return 2;
		} else if (d.getMonat() <= 9 && d.getMonat() > 6) {
			return 3;
		} else if (d.getMonat() <= 12 && d.getMonat() > 9) {
			return 4;
		} else
			return -1;
	}
	
	
	//public static int berechneQuartal3(Datum d) {


	
	//	return d.getMonat() / 3 + 1;
		
	//}
	
	
	
			

	public static int berechneQuartal(Datum d1) {

		int quartal = 0;
		switch (d1.getMonat()) {

		case 1:
		case 2:
		case 3:

			quartal = 1;

			break;

		case 4:
		case 5:
		case 6:

			quartal = 2;

			break;

		case 7:
		case 8:
		case 9:

			quartal = 3;

			break;

		case 10:
		case 11:
		case 12:

			quartal = 1;

			break;

		}
		return quartal;

	}

}
