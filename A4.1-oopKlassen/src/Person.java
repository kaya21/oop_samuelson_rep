
public class Person {

	private String vorname;
	private String nachname;
	private Adresse adr;
	//private static String verwalt;
	//private String zuweis;
	
	public Person(String vorname, String nachname, Adresse adr) {
		
		this.vorname = vorname;
		this.nachname = nachname;
		this.adr = adr;
		//this.verwalt = verwalt;
		//this.zuweis = zuweis;
		
	}
	
	
	@Override
	public String toString() {
		
		return 	"\n Vorname: " +vorname + " , Nachname: " + nachname + 
			//	"\n [Straße: " +adr.getStr() + " , Hausnummer: " +adr.getHNr() +
			//	" , PLZ: " + adr.getPLZ() + ", Ort: " +adr.getOrt() + "]";
				"\n " +adr.toString() + "\n";

	}
	
	
	
	
	
	public String liefStr() {
		
		return adr.getStr();
	}
	
	public String getVorname() {
		return this.vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getNachname() {
		return this.vorname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	
	public void setAdresse(Adresse adr) {
		this.adr = adr;
	}
	
	public Adresse getAdresse() {
		return adr;
	}
	
	
	

	
}
