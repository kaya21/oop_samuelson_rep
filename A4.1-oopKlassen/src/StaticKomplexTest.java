
public class StaticKomplexTest {

	public static void main(String[] args) {

		KomplexeZahl k1 = new KomplexeZahl(2, -3);
		KomplexeZahl k2 = new KomplexeZahl();
		
		KomplexeZahl k = KomplexeZahl.multi(k1, k2);
		
		
		double wertArray[] = BerechneKomplex(k1, k2);

		System.out.println(k1);
		System.out.printf("%+.1f %+.1fj\n", k1.getRe(), k1.getIm());
		System.out.println(k2);
		
		System.out.printf("Ergebnis: %.1f %.1fj\n", wertArray[0],wertArray[1]);
		
		System.out.println(k);
		
	}

	public static double[] BerechneKomplex(KomplexeZahl k1, KomplexeZahl k2) {

		double[] wertArray = new double[2];
		
		
		wertArray[0] = k1.getRe() * k2.getRe() - k1.getIm() * k2.getIm();
		wertArray[1] = k1.getRe() * k2.getIm() + k1.getIm() * k2.getRe();

		return wertArray;

	}

}
