
public class Buch {
	
	private int buchnummer;
	private String titel;
	
	
	public static int nextNumber = 1000; //Static = Klassen Attribute ohne Static Object Atribut
	
	public Buch(String titel) {
		
		
		setBuchnummer(nextNumber);
		setTitel(titel);
		
		nextNumber++;
		
	}
	
	public int getNextNumber() {
		
		return nextNumber;
	}

	public int getBuchnummer() {
		return buchnummer;
	}

	public void setBuchnummer(int buchnummer) {
		this.buchnummer = buchnummer;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}
	
	@Override
	public String toString() {
		return "(Buchnummer: " +buchnummer +", Titel: " +titel + ")";
	}
	
	

}
