	import java.util.Scanner;
	
public class SummeForSchleife {



		static Scanner myScanner = new Scanner(System.in);

		public static void main(String[] args) {

			int eingabe;

			eingabe = eingabeInt("Bitte geben Sie eine Zahl an:");
			berechneSum(eingabe);
			berechneSumG(eingabe);
//			berechneSumU(eingabe);
		}

		public static int eingabeInt(String eingabe) {

			System.out.println(eingabe);
			return myScanner.nextInt();

		}
		
		public static void berechneSum(int zahl) {
			
			int folge = 0;
			int sum = 0;
			
			
			System.out.printf("%s %24s", "Zahlenfolge:", " ");
			for (int i=0; i<zahl;i++) {
				
				folge = i+1;
				System.out.printf("%d ", folge);
				
				sum = sum+i+1;
		
			}
			
			System.out.printf("\nDie Summe der Zahlenfolge beträgt: %4d ", sum);
			
		}
		
		public static void berechneSumG(int zahl) {
			
			int folge = 0;
			int sum = 0;
			
			
			System.out.printf("\n%s %24s", "Zahlenfolge:", " ");
			for (int i=0; i<zahl;i++) {
				
				folge = i+1;
				System.out.printf("%d ", folge*2);
				
				sum = sum+i+1;
		
			}
			
			System.out.printf("\nDie Summe der Zahlenfolge beträgt: %4d ", sum*2);
			
		}
		

		
		
		
	}