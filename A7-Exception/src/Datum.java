
public class Datum {

	private int tag;
	private int monat;
	private int jahr;

	public Datum(int tag, int monat, int jahr) throws TagException, MonatException, JahrException {

		settag(tag);
		setmonat(monat);
		setjahr(jahr);
	}

	public Datum() throws TagException, MonatException, JahrException {

		settag(132);
		setmonat(130);
		setjahr(19371);

	}

	public void settag(int tag) throws TagException {
		if (tag > 0 && tag < 32)
			this.tag = tag;
		else
			throw new TagException("Tag ungültig", tag);

	}

	public int gettag() {
		return tag;
	}

	public void setmonat(int monat) throws MonatException {
		if (monat > 0 && monat <= 12)
			this.monat = monat;
		else
			throw new MonatException("Monat ungültig", monat);
	}

	public int getmonat() {
		return monat;
	}

	public void setjahr(int jahr) throws JahrException {
		if (jahr > 1900 && jahr <= 2100)
			this.jahr = jahr;
		else
			throw new JahrException("Jahr ungültig", jahr);
	}

	public int getjahr() {
		return jahr;
	}

	public String berechneQuartal(int monat) {

		if (monat <= 3)
			return "Q1";
		if (monat > 3 && monat <= 6)
			return "Q2";
		if (monat > 6 && monat <= 9)
			return "Q3";
		if (monat > 9 && monat <= 12)
			return "Q4";
		else
			return "false";
	}

	public String toString() {
		return this.tag + "." + this.monat + "." + this.jahr;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj instanceof Datum) {
			Datum dtemp = (Datum) obj;
			return this.tag == dtemp.gettag() && this.monat == dtemp.getmonat() && this.jahr == dtemp.getjahr();
		}
		return false;

	}

}
