
public class DatumException extends Exception{
	
private int value;
	
	public DatumException(String msg, int value) {
		super(msg);
		this.value = value;
		

	}
	
	public int getDatum() {
		return value;
	}

}
