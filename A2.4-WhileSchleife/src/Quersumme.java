import java.util.Scanner;

public class Quersumme {

	static Scanner myScanner = new Scanner(System.in);

	public static void main(String[] args) {

		int eingabe;

		eingabe = eingabeInt("Bitte geben Sie eine Zahl an:");
		berechneQsum(eingabe);
	}

	public static int eingabeInt(String eingabe) {

		System.out.println(eingabe);
		return myScanner.nextInt();

	}
	
	public static void berechneQsum(int zahl) {
		
		int summe = 0;
		
		while (zahl != 0) {
			
			summe = summe + (zahl % 10);
			
			zahl = zahl / 10;
		}
		System.out.printf("Die Quersumme Beträgt: %d", summe);
		
	}
	
	
	
}
