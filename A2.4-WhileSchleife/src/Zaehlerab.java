import java.util.Scanner;

public class Zaehlerab {

	static Scanner myScanner = new Scanner(System.in);

	public static void main(String[] args) {

		int eingabe;

		eingabe = eingabeInt("Bitte geben Sie eine ganze Zahl an:");
		zaehlerIntBack(eingabe);
		zaehlerIntFor(eingabe);

	}

	public static int eingabeInt(String eingabe) {

		System.out.println(eingabe);
		return myScanner.nextInt();

	}

	public static void zaehlerIntBack(int eingabe) {

		System.out.printf("%d ", eingabe);

		while (eingabe > 0) {

			eingabe = eingabe - 1;
			System.out.printf("%d ", eingabe);

		}

	}

	public static void zaehlerIntFor(int eingabe) {

		int k = 0;

		System.out.printf("\n%d ", k);

		while (eingabe != 0) {

			k += 1;
			System.out.printf("%d ", k);
			eingabe--;
		}

	}

}
