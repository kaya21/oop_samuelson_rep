import java.util.Scanner;

public class Zinsrechner {

	static Scanner myScanner = new Scanner(System.in);	

	public static void main(String[] args) {

		int laufzeit, kapital;
		float zinssatz, ergebnis;

		laufzeit = 	leseInt("Bitte geben Sie die gewünchte Laufzeit ein:");
		kapital = 	leseInt("Welcher Betrag soll angelegt werden?");
		zinssatz = 	leseFloat("Welcher Zinssatz?");
		ergebnis = 	berechnungEndbetrag(laufzeit, kapital, zinssatz);

		System.out.printf("Eingezahltes Kapital: %d\n", kapital);
		System.out.printf("Zinssatzbeträgt: %.2f\n", zinssatz);
		System.out.printf("Ausgezahltes Kapital: %.2f\n", ergebnis);

	}

	public static int leseInt(String text) {

		System.out.println(text);
		return myScanner.nextInt();
	}

	public static float leseFloat(String text) {

		System.out.println(text);
		return myScanner.nextFloat();
	}

	public static float berechnungEndbetrag(int laufzeit, int betrag, float zinssatz) {

		
		int k = 0;
		float zinsen = zinssatz / 100;
		float zErgebnis = betrag;
		
		while (k < laufzeit) {

			zErgebnis = zErgebnis * (1 + zinsen);

			k++;

		}

		return zErgebnis;

	}

}