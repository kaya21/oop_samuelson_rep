
public class Kreis extends GeometrischeObjekte {
	
	private double radius;
	
	
	

	public Kreis(String farbe, int xpos, int ypos, double radius) { //Konstruktor
		super(farbe, xpos, ypos);
		this.radius = radius;
	}
	
	@Override
	public void zeichne() {
		
		System.out.println("Kreis zeichenen");
	}
	
	
	@Override
	public String toString() {
		return super.toString() + ", " +radius;
	}

}
